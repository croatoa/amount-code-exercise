﻿using Amount.CodeExercise.Core.Models.Shared;

namespace Amount.CodeExercise.Core.Models.Demographic
{
    public class SocialSecurityNumber : AsValue<string>
    {
        public const int Exact = 9;
        public const int Min = 100000000;
        public const int Max = 999999999;

        public static implicit operator string(SocialSecurityNumber ssn)
        {
            return ssn.Value ?? string.Empty;
        }

        public static implicit operator SocialSecurityNumber(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException(nameof(Name), "Cannot cast null to Name");
            }
            if (value.Length != Exact)
            {
                throw new ArgumentException($"SSN must be {Exact} characters");
            }
            return new SocialSecurityNumber()
            {
                Value = value
            };
        }
    }
}
