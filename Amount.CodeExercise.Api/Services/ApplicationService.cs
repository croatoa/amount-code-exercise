﻿using Amount.CodeExercise.Api.DataTransferObjects;
using Amount.CodeExercise.Api.Interfaces;
using Amount.CodeExercise.Api.Setup;
using Amount.CodeExercise.Core.Models.Business;
using Amount.CodeExercise.Core.Models.Demographic;
using Amount.CodeExercise.Core.Models.Loan;
using Microsoft.Extensions.Logging;

namespace Amount.CodeExercise.Api.Services
{
    public class ApplicationService : IApplicationService
    {
        private IRepository<Data.Tables.Loan> _loanRepository;
        private IRepository<Data.Tables.Organization> _organizationRepository;
        private IRepository<Data.Tables.Person> _personRepository;
        private ILogger<ApplicationService> _logger;
        private ICache<Application> _applicationCache;

        public ApplicationService(
          IRepository<Data.Tables.Loan> loanRepository,
          IRepository<Data.Tables.Organization> organizationRepository,
          IRepository<Data.Tables.Person> personRepository,
          ILogger<ApplicationService> logger
        )
        {
            _loanRepository = loanRepository;
            _organizationRepository = organizationRepository;
            _personRepository = personRepository;
            _logger = logger;

            _applicationCache = new Cache<Application>();
        }

        public async Task<Application> GetApplicationAsync(int loanId)
        {
            try
            {
                var loan = await _loanRepository.Select(loanId);
                var organization = await _organizationRepository.Select(loan.OrganizationId);
                var person = await _personRepository.Select(loan.PersonId);

                return new Application(loan.ToModel(), organization.ToModel(), person.ToModel());
            }
            catch (Exception exception)
            {
                _logger.LogError($"Error getting application for loan {loanId}", exception);
                throw;
            }
        }

        static class SortTable  
        {
            private static ISort<Data.Tables.Loan, string> _loanSortString = null;
        }
        public async Task<List<Application>> GetApplicationsAsync(string sortField = "RiskRating", string direction = "asc")
        {
            try
            {
                if (_applicationCache.IsSet)
                {
                    return _applicationCache.Read().SortDtos(sortField,direction);
                }
                else
                {

                    ISort<Data.Tables.Loan, decimal> sort = new Sort<Data.Tables.Loan, decimal>(loan => loan.AnnualPercentageRate,false);
                    var tasks = new Task[3]
                    {
                         _loanRepository.SelectAll(),
                         _organizationRepository.SelectAll(),
                         _personRepository.SelectAll()
                    };

                    await Task.WhenAll(tasks);

                    var loans = ((Task<List<Data.Tables.Loan>>)tasks[0]).Result;
                    var orgs = ((Task<List<Data.Tables.Organization>>)tasks[1]).Result.ToDictionary(org => org.Id);
                    var persons = ((Task<List<Data.Tables.Person>>)tasks[2]).Result.ToDictionary(person => person.Id);

                    return loans
                      .Select(loan =>
                        new Application(
                          loan.ToModel(),
                          orgs.ContainsKey(loan.OrganizationId) ? orgs[loan.OrganizationId].ToModel() : new Organization(),
                          persons.ContainsKey(loan.PersonId) ? persons[loan.PersonId].ToModel() : new Person()
                        )
                      ).ToList().SortDtos(sortField, direction);
                }
            }
            catch (Exception exception)
            {
                _logger.LogError($"Error getting all applications", exception);
                throw;
            }
        }        
        public async Task CreateApplicationAsync(Application application)
        {
            try
            {
                var person =
                  await _personRepository.SelectFirst(p => application.SignerSocialSecurityNumber == p.SocialSecurityNumber) ??
                  await _personRepository.Insert(
                    Data.Tables.Person.FromModel(
                    new Person()
                    {
                        FirstName = application.SignerFirstName,
                        LastName = application.SignerLastName,
                        DateOfBirth = application.SignerDateOfBirth,
                        SocialSecurityNumber = application.SignerSocialSecurityNumber
                    }
                  )
                );
                var organization =
                  await _organizationRepository.SelectFirst(org => application.EmployeeIdentificationNumber == org.EmployeeIdentificationNumber) ??
                  await _organizationRepository.Insert(
                    Data.Tables.Organization.FromModel(
                    new Organization()
                    {
                        Name = application.OrganizationName,
                        CreditRating = application.CreditRating,
                        EmployeeIdentificationNumber = application.EmployeeIdentificationNumber,
                        OutstandingDebt = application.OutstandingDebt,
                        RecentDefaults = application.RecentDefaults,
                        Revenue = application.Revenue,
                    })
                  );

                var loan = await _loanRepository.Insert(
                  Data.Tables.Loan.FromModel(
                  new Loan()
                  {
                      AnnualPercentageRate = application.AnnualPercentageRate,
                      RequestedAmount = application.RequestedAmount,
                      OrganizationId = organization.Id,
                      PersonId = person.Id,
                      Term = application.Term
                  })
                );
                _applicationCache.Invalidate();
            }
            catch (Exception exception)
            {
                _logger.LogError($"Error creating application", exception);
                throw;
            }
        }

        public async Task EditApplicationAsync(Application application)
        {
            try
            {
                var existingLoan = (await _loanRepository.Select(application.LoanId))?.ToModel();
                if (existingLoan == null)
                {
                    await CreateApplicationAsync(application);
                    return;
                }

                var existingPerson = (await _personRepository.Select(existingLoan.PersonId))?.ToModel();
                var existingOrg = (await _organizationRepository.Select(existingLoan.OrganizationId))?.ToModel();

                if (existingOrg == null || existingPerson == null)
                {
                    throw new Exception("Person or organization not found; unable to edit");
                }

                existingLoan.Term = application.Term;
                existingLoan.AnnualPercentageRate = application.AnnualPercentageRate;
                existingLoan.RequestedAmount = application.RequestedAmount;

                existingPerson.DateOfBirth = application.SignerDateOfBirth;
                existingPerson.LastName = application.SignerLastName;
                existingPerson.FirstName = application.SignerFirstName;
                existingPerson.SocialSecurityNumber = application.SignerSocialSecurityNumber;

                existingOrg.EmployeeIdentificationNumber = application.EmployeeIdentificationNumber;
                existingOrg.Name = application.OrganizationName;
                existingOrg.RecentDefaults = application.RecentDefaults;
                existingOrg.OutstandingDebt = application.OutstandingDebt;
                existingOrg.Revenue = application.Revenue;
                existingOrg.CreditRating = application.CreditRating;

                await Task.WhenAll(
                  _loanRepository.Update(Data.Tables.Loan.FromModel(existingLoan)),
                  _personRepository.Update(Data.Tables.Person.FromModel(existingPerson)),
                  _organizationRepository.Update(Data.Tables.Organization.FromModel(existingOrg))
                );

                _applicationCache.Invalidate();
            }
            catch (Exception exception)
            {
                _logger.LogError($"Error updating application", exception);
                throw;
            }
        }
        public async Task DeleteApplicationAsync(int loanId)
        {
            try
            {
                await _loanRepository.Delete(loanId);
                _applicationCache.Invalidate();
            }
            catch (Exception exception)
            {
                _logger.LogError($"Error deleting application", exception);
                throw;
            }
        }

        public async Task DeleteApplicationAsync()
        {
            try
            {
                await _loanRepository.WipeClean();
                _applicationCache.Invalidate();
            }
            catch (Exception exception)
            {
                _logger.LogError($"Error deleting all applications", exception);
                throw;
            }
        }

        public async Task SeedData()
        {
            try
            {
                await _loanRepository.WipeClean();
            }
            catch (Exception exception)
            {
                _logger.LogError($"Error emptying database tables", exception);
                throw;
            }
            _logger.LogInformation("(re)seeding data");
            try
            {
                new Seeder(1000)
                  .GeneratePersons(out List<Person> persons)
                  .GenerateOrganizations(out List<Organization> organizations)
                  .SetIds(
                    (await _personRepository.InsertSet(persons.Select(p => Data.Tables.Person.FromModel(p)).ToList())).Select(p => p.Id),
                    (await _organizationRepository.InsertSet(organizations.Select(o => Data.Tables.Organization.FromModel(o)).ToList())).Select(o => o.Id)
                  ).GenerateLoans(out List<Loan> loans);

                await _loanRepository.InsertSet(loans.Select(l => Data.Tables.Loan.FromModel(l)).ToList());
                _applicationCache.Invalidate();
            }
            catch (Exception exception)
            {
                _logger.LogError($"Error seeding data", exception);
                throw;
            }
        }

        public async Task<List<Application>> Search(Query query)
        {
            if(query == null)
            {
                return Default.Applications;
            }
            else if (!string.IsNullOrWhiteSpace(query.Field))
            {
                return (await FieldSearch(query.Field, query.Keywords, query.Min, query.Max, query.Exact)).SortDtos(query.Sort,query.Direction);
            }
            else if (query.Keywords.Any())
            {
                return (await OpenSearch(query.Keywords)).SortDtos(query.Sort, query.Direction);
            }
            else
            {
                return Default.Applications;
            }
        }

        private async Task<List<Application>> OpenSearch(string[] terms)
        {
            return (await GetApplicationsAsync()).Where(a => terms.Any(t => a.SearchText.Contains(t))).ToList();
        }

        private List<Application> SearchRiskRating(decimal? min, decimal? max, decimal? exact)
        {
            if(exact == null)
            {
                if (min == null) min = 0m;
                if (max == null) max = decimal.MaxValue;
                return _applicationCache.Read().Where(dto => dto.RiskRating >= min && dto.RiskRating <= max).ToList();
            }
            else
            {
                return _applicationCache.Read().Where(dto => dto.RiskRating == exact).ToList();
            }
        }

        private async Task<List<Application>> FieldSearch(
          string field,
          string[] terms,
          decimal? min,
          decimal? max,
          decimal? exact
        )
        {
            if (terms.Length == 0 && min == null && max == null && exact == null)
            {
                return Default.Applications;
            }
            else
            {
                field = field.Replace("organization", string.Empty).Replace("signer",string.Empty);
                if(field == "riskrating") 
                { 
                    return SearchRiskRating(min,max,exact);
                }
                else if (HasField(typeof(Loan), field))
                {
                    var loans = await _loanRepository.FieldSearch(field, terms, min, max, exact);
                    var orgIds = loans.Select(l => l.OrganizationId);
                    var personIds = loans.Select(l => l.PersonId);
                    var orgs = (await _organizationRepository.SelectWhere(o => orgIds.Contains(o.Id))).ToDictionary(o => o.Id);
                    var persons = (await _personRepository.SelectWhere(p => personIds.Contains(p.Id))).ToDictionary(p => p.Id);
                    return loans
                     .Select(loan =>
                       new Application(
                         loan.ToModel(),
                         orgs.ContainsKey(loan.OrganizationId) ? orgs[loan.OrganizationId].ToModel() : new Organization(),
                         persons.ContainsKey(loan.PersonId) ? persons[loan.PersonId].ToModel() : new Person()
                       )
                     )
                     .ToList();
                }
                else if (HasField(typeof(Organization), field))
                {
                    var orgs = (await _organizationRepository.FieldSearch(field, terms, min, max, exact)).ToDictionary(o => o.Id);
                    var orgIds = orgs.Values.Select(o => o.Id);
                    var loans = await _loanRepository.SelectWhere(l => orgIds.Contains(l.OrganizationId));
                    var personIds = loans.Select(l => l.PersonId);
                    var persons = (await _personRepository.SelectWhere(p => personIds.Contains(p.Id))).ToDictionary(p => p.Id);
                    return loans
                     .Select(loan =>
                       new Application(
                         loan.ToModel(),
                         orgs.ContainsKey(loan.OrganizationId) ? orgs[loan.OrganizationId].ToModel() : new Organization(),
                         persons.ContainsKey(loan.PersonId) ? persons[loan.PersonId].ToModel() : new Person()
                       )
                     ).ToList();
                }
                else if (HasField(typeof(Person), field))
                {
                    var persons = (await _personRepository.FieldSearch(field, terms, min, max, exact)).ToDictionary(p => p.Id);
                    var personIds = persons.Values.Select(p => p.Id);
                    var loans = await _loanRepository.SelectWhere(l => personIds.Contains(l.PersonId));
                    var orgIds = loans.Select(l => l.OrganizationId);
                    var orgs = (await _organizationRepository.SelectWhere(o => orgIds.Contains(o.Id))).ToDictionary(o => o.Id);
                    return loans
                     .Select(loan =>
                       new Application(
                         loan.ToModel(),
                         orgs.ContainsKey(loan.OrganizationId) ? orgs[loan.OrganizationId].ToModel() : new Organization(),
                         persons.ContainsKey(loan.PersonId) ? persons[loan.PersonId].ToModel() : new Person()
                       )
                     ).ToList(); 
                }
                else
                {
                    return Default.Applications;
                }
            }
        }
        

        private bool HasField(Type type, string name)
        {
            return type.GetProperties().Any(p => name.ToLower() == p.Name.ToLower());
        }

        internal static class Default
        {
            internal static List<Application> Applications = new List<Application>();
        }
    }
}
