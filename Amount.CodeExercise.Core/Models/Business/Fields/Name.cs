﻿using Amount.CodeExercise.Core.Models.Shared;

namespace Amount.CodeExercise.Core.Models.Business
{
    public class Name : AsValue<string>
    {
        public const int Min = 2;
        public const int Max = 50;

        public static implicit operator string(Name name)
        {
            return name.Value ?? string.Empty;
        }

        public static implicit operator Name(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException(nameof(Name), "Cannot cast null to Name");
            }
            if (value.Length < Min)
            {
                throw new ArgumentException($"Name cannot be less than {Min} characters");
            }
            if (value.Length > Max)
            {
                throw new ArgumentException($"Name cannot be greater than {Max} characters");
            }
            return new Name()
            {
                Value = value
            };
        }
    }
}
