﻿using Amount.CodeExercise.Core.Models.Business;
using Amount.CodeExercise.Core.Models.Demographic;
using Amount.CodeExercise.Core.Models.Loan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amount.CodeExercise.Api.Setup
{
  internal class Seeder
  {
    private int _count;
    private int _step = 0;
    private List<Person> _persons = new List<Person>();
    private List<Organization> _orgs = new List<Organization>();
    private List<Loan> _loans = new List<Loan>();

    private List<int> _personIds = new List<int>();
    private List<int> _organizationIds = new List<int>();

    public Seeder(int count)
    {
      _count = count;
    }

    public Seeder GeneratePersons(out List<Person> persons)
    {
      if(_step == 0)
      {
        for(var i = 0; i < _count; i++)
        {
          _persons.Add(
            new Person()
            {
              FirstName = Random.Name(),
              LastName = Random.Name(),
              DateOfBirth = Random.DateOfBirth(),
              SocialSecurityNumber = Random.TaxId()
            }
          ); ;
        }

        _step += 1;
      }
      persons = _persons;
      return this;
    }
    public Seeder GenerateOrganizations(out List<Organization> organizations)
    {
      if (_step == 1)
      {
        for (var i = 0; i < _count; i++)
        {
          _orgs.Add(new Organization() 
          { 
            CreditRating = Random.Rating(),
            EmployeeIdentificationNumber = Random.TaxId(),
            Name = $"{Random.Name()}, Inc.",
            OutstandingDebt = Random.Debt(),
            RecentDefaults = Random.Defaults(),
            Revenue = Random.Revenue()
          });
        }
        _step += 1;
      }
      organizations = _orgs;
      return this;
    }

    public Seeder SetIds(IEnumerable<int> personIds, IEnumerable<int> organizationIds)
    {
      if (_step == 2)
      {
        _personIds = personIds.ToList();
        _organizationIds = organizationIds.ToList();
        _step += 1;
      }
      return this;    
    }

    public Seeder GenerateLoans(out List<Loan> loans)
    {
      if (_step == 3)
      {
        for(var i = 0; i < _count; i += 1)
        {
          _loans.Add(new Loan() 
          { 
            AnnualPercentageRate = Random.Apr(),
            RequestedAmount = Random.Request(),
            OrganizationId = _organizationIds[i],
            PersonId = _personIds[i],
            Term = Random.Term()
          });
        }
        _step += 1;
      }
      loans = _loans;
      return this;
    }

    static class Random
    {
      static System.Random _gen = new System.Random();
      static DateTime _date = DateTime.Today;
      static string[] _consanants = new string[21] { "q", "w", "r", "t", "y", "p", "s", "d", "f", "g", "h", "j", "k", "l", "z", "x", "c", "v", "b", "n", "m" };
      static string[] _vowels = new string[6] { "a", "e", "i", "o", "u", "y" };

      public static string Name()
      {
        var output = string.Empty;
        var syllables = _gen.Next(1, 3);
        for (var i = 0; i < syllables; i += 1)
        {
          output += (_gen.Next(0, 2) == 0 ? _vowels[_gen.Next(0, 5)] : _consanants[_gen.Next(0, 20)]) +
                    (_gen.Next(0, 2) == 0 ? _consanants[_gen.Next(0, 20)] : _vowels[_gen.Next(0, 5)]) +
                    (_gen.Next(0, 2) == 0 ? _vowels[_gen.Next(0, 5)] : _consanants[_gen.Next(0, 20)]);
        }
        return output.Substring(0,1).ToUpper() + output.Substring(1);
      }

      public static DateTime DateOfBirth()
      {
        return new DateTime(_gen.Next(_date.Year - 100, _date.Year - 18), _gen.Next(1, 12), _gen.Next(1, 28));
      }

      public static string TaxId()
      {
        var output = _gen.Next(1, 9).ToString();
        for (var i = 0; i < 8; i += 1)
        {
          output += _gen.Next(0, 9);
        }
        return output;
      }

      public static int Rating()
      {
        return _gen.Next(CreditRating.Min, CreditRating.Max);
      }

      public static decimal Debt()
      {
        return NextDecimal(OutstandingDebt.Min, OutstandingDebt.Max);
      }

      public static int Defaults()
      {
        return _gen.Next(RecentDefaults.Min, RecentDefaults.Max);
      }

      public static decimal Revenue()
      {
        return NextDecimal(Core.Models.Business.Revenue.Min, Core.Models.Business.Revenue.Max);
      }

      public static decimal Apr()
      {
        return NextDecimal(AnnualPercentageRate.Min, AnnualPercentageRate.Max);
      }

      public static decimal Request()
      {
        return NextDecimal(RequestedAmount.Min, RequestedAmount.Max);
      }

      public static int Term()
      {
        return _gen.Next(Core.Models.Loan.Term.Min, Core.Models.Loan.Term.Max);
      }

      private static decimal NextDecimal(decimal lower, decimal upper)
      {
        return (decimal)(_gen.Next((int)lower, (int)upper)) + ((decimal)_gen.Next(0,100)/100m);
      }
    }
  }
}
