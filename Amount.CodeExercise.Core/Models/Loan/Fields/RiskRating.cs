﻿using Amount.CodeExercise.Core.Models.Shared;

namespace Amount.CodeExercise.Core.Models.Loan
{
    public class RiskRating : AsValue<decimal>
    {
        public static implicit operator decimal(RiskRating riskRating)
        {
            return riskRating.Value;
        }

        public static implicit operator RiskRating(decimal value)
        {
            if (value < 0m)
            {
                throw new ArgumentOutOfRangeException(nameof(RiskRating), "Cannot be less than 0");
            }
            return new RiskRating()
            {
                Value = value
            };
        }
    }
}
