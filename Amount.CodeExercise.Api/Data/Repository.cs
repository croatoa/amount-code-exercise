﻿using Amount.CodeExercise.Api.Interfaces;
using Amount.CodeExercise.Core.Models.Shared;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Amount.CodeExercise.Api.Data
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private CodeExerciseContext _context;
        private System.Reflection.PropertyInfo[] _props;
        public Repository(CodeExerciseContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
            _props = typeof(T).GetProperties();
        }

        public async Task<T> Select(int id)
        {
            return await _context.FindAsync<T>(id);
        }

        public async Task<List<T>> Select(List<int> ids)
        {
            return _context.Set<T>().Where(record => ids.Contains(BaseModel.GetId(record))).ToList();
        }

        public async Task<List<T>> SelectAll()
        {
            return await Task.FromResult(_context.Set<T>().ToList());
        }

/*        public async Task<List<T>> SelectAll<TSortField>(Sort<T,TSortField> sort)
        {
            if(sort == null || sort.TableType != typeof(T))
            {
                return await Task.FromResult(_context.Set<T>().ToList());
            }
            else if (sort.IsDescending)
            {
                return await Task.FromResult(_context.Set<T>().OrderByDescending(sort.Lambda).ToList());
            }
            else
            {
                return await Task.FromResult(_context.Set<T>().OrderBy(sort.Lambda).ToList());
            }
        }*/
        public async Task<List<T>> SelectWhere(Func<T, bool> filter)
        {
            return await Task.FromResult(_context.Set<T>().Where(filter).ToList());
        }

        public async Task<T> SelectFirst(Func<T, bool> filter)
        {
            return await Task.FromResult(_context.Set<T>().FirstOrDefault(filter));
        }

        public async Task<T> Insert(T model)
        {
            var result = await _context.AddAsync<T>(model);
            await _context.SaveChangesAsync();
            return result.Entity as T;
        }

        public async Task<List<T>> InsertSet(List<T> models)
        {
            await _context.AddRangeAsync(models);
            await _context.SaveChangesAsync();
            return models;
        }

        public async Task Update(T model)
        {
            var existing = await _context.FindAsync<T>(Convert.ToInt32(typeof(T).GetProperty("Id")?.GetValue(model)));
            _context.Entry<T>(existing).CurrentValues.SetValues(model);
            _context.Entry<T>(existing).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<List<T>> UpdateSet(List<T> models)
        {
            _context.UpdateRange(models);
            await _context.SaveChangesAsync();
            return models;
        }

        public async Task Delete(int id)
        {
            var entity = await _context.FindAsync<T>(id);
            if (entity != null)
            {
                _context.Remove<T>(entity);
            }
            await _context.SaveChangesAsync();
        }

        public async Task WipeClean()
        {
            _context.RemoveRange(_context.Loans);
            await _context.SaveChangesAsync();

            _context.RemoveRange(_context.Organizations);
            await _context.SaveChangesAsync();

            _context.RemoveRange(_context.Persons);
            await _context.SaveChangesAsync();
        }
        public async Task<List<T>> FieldSearch(
          string field,
          string[] terms,
          decimal? min,
          decimal? max,
          decimal? exact
        )
        {
            var conditions = new List<string>();
            if (terms.Any())
            {
                var termSet = new List<string>();
                foreach (var term in terms)
                {
                    termSet.Add($"charindex('{term}', cast ({field} as varchar)) > 0");
                }
                conditions.Add($"({string.Join(" or ", termSet)})");
            }            
            

            if (exact == null)
            {
                if (min != null)
                {
                    conditions.Add($"{field} >= {min}");
                }
                if (max != null)
                {
                    conditions.Add($"{field} <= {max}");
                }
            }
            else
            {
                conditions.Add($"{field} = {exact}");
            }

            if (conditions.Count == 0)
            {
                return new List<T>();
            }

            return await Task.FromResult(
              _context
                .Set<T>()
                .FromSqlRaw<T>(@$"select * 
from [dbo].[{typeof(T).Name}s] 
where {string.Join(" and ", conditions)}"
              ).ToList()
            );       
        }
    }
}
