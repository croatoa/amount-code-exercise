﻿using Amount.CodeExercise.Api.Data.Tables;
using Microsoft.EntityFrameworkCore;

namespace Amount.CodeExercise.Api.Data
{
    public class CodeExerciseContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }
        public DbSet<Loan> Loans { get; set; }
        public DbSet<Organization> Organizations { get; set; }

        public CodeExerciseContext(DbContextOptions<CodeExerciseContext> options) : base(options)
        {

        }

    }
}
