﻿using Amount.CodeExercise.Core.Models.Shared;

namespace Amount.CodeExercise.Core.Models.Demographic
{
    public class Name : AsValue<string>
    {
        public const int Min = 2;
        public const int Max = 26;
        public static implicit operator string(Name name)
        {
            return name.Value ?? string.Empty;
        }

        public static implicit operator Name(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException(nameof(Name), "Cannot cast null to Name");
            }
            if (value.Length < 2)
            {
                // ISO IEC 7813
                throw new ArgumentException($"Name cannot have fewer than {Min} characters");
            }
            if (value.Length > 26)
            {
                // ISO IEC 7813
                throw new ArgumentException($"Name cannot have more than {Max} characters");
            }
            return new Name()
            {
                Value = value
            };
        }
    }
}
