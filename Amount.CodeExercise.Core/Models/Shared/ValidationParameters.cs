﻿using Amount.CodeExercise.Core.Models.Business;
using Amount.CodeExercise.Core.Models.Demographic;
using Amount.CodeExercise.Core.Models.Loan;

namespace Amount.CodeExercise.Core.Models.Shared
{
    public class ValidationParameters
    {
        // person
        public int PersonNameMin => Demographic.Name.Min;
        public int PersonNameMax => Demographic.Name.Max;
        public string DateOfBirthMin => $"{DateTime.Now.Year-110}-01-01";
        public string DateOfBirthMax => $"{DateTime.Now.Year - 17}-01-01";
        public int SocialSecurityNumberMin => SocialSecurityNumber.Min;
        public int SocialSecurityNumberMax => SocialSecurityNumber.Max;
        // organization
        public int CreditRatingMin => CreditRating.Min;
        public int CreditRatingMax => CreditRating.Max;
        public int OrganizationNameMin => Business.Name.Min;
        public int OrganizationNameMax => Business.Name.Max;
        public decimal OutstandingDebtMin => OutstandingDebt.Min;
        public decimal OutstandingDebtMax => OutstandingDebt.Max;
        public int RecentDefaultsMin => RecentDefaults.Min;
        public int RecentDefaultsMax => RecentDefaults.Max;
        public decimal RevenueMin => Revenue.Min;
        public decimal RevenueMax => Revenue.Max;
        public int TaxIdMin => TaxId.Min;
        public int TaxIdMax => TaxId.Max;
        // loan
        public decimal AnnualPercentageRateMin => AnnualPercentageRate.Min;
        public decimal AnnualPercentageRateMax => AnnualPercentageRate.Max;
        public decimal RequestedAmountMin => RequestedAmount.Min;
        public decimal RequestedAmountMax => RequestedAmount.Max;
        public int TermMin => Term.Min;
        public int TermMax => Term.Max;
    }
}
