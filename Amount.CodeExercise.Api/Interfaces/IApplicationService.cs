﻿using Amount.CodeExercise.Api.DataTransferObjects;

namespace Amount.CodeExercise.Api.Interfaces
{
    public interface IApplicationService
    {
        Task<Application> GetApplicationAsync(int loanId);
        Task<List<Application>> GetApplicationsAsync(string sortField = "RiskRating", string direction = "asc");
        Task CreateApplicationAsync(Application application);
        Task EditApplicationAsync(Application application);
        Task DeleteApplicationAsync(int loanId);
        Task DeleteApplicationAsync();
        Task SeedData();
        Task<List<Application>> Search(Query query);
    }
}
