﻿using Amount.CodeExercise.Core.Models.Shared;

namespace Amount.CodeExercise.Core.Models.Loan
{
    public class RequestedAmount : AsValue<decimal>
    {

        public const decimal Min = 25000m;
        public const decimal Max = 1000000m;
        public static implicit operator decimal(RequestedAmount RequestedAmount)
        {
            return RequestedAmount.Value;
        }

        public static implicit operator RequestedAmount(decimal value)
        {
            if (value < Min)
            {
                throw new ArgumentOutOfRangeException(nameof(RequestedAmount), $"Cannot be less than {Min:c}");
            }
            if (value > Max)
            {
                throw new ArgumentOutOfRangeException(nameof(RequestedAmount), $"Cannot be more than {Max:c}");
            }
            return new RequestedAmount()
            {
                Value = value
            };
        }
    }
}
