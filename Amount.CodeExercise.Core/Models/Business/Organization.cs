﻿using Amount.CodeExercise.Core.Models.Shared;

namespace Amount.CodeExercise.Core.Models.Business
{
    public class Organization : BaseModel
    {
        public Name Name { get; set; }
        // public Contact Contact { get; set; }
        public TaxId EmployeeIdentificationNumber { get; set; }
        public CreditRating CreditRating { get; set; }
        public OutstandingDebt OutstandingDebt { get; set; }
        public RecentDefaults RecentDefaults { get; set; }
        public Revenue Revenue { get; set; }
    }
}
