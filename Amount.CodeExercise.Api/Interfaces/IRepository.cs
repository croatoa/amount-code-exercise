﻿namespace Amount.CodeExercise.Api.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<T> Select(int id);
        Task<List<T>> Select(List<int> ids);
        Task<List<T>> SelectAll();
        Task<List<T>> SelectWhere(Func<T, bool> filter);
        Task<T> SelectFirst(Func<T, bool> filter);
        Task<T> Insert(T model);
        Task<List<T>> InsertSet(List<T> model);
        Task Update(T model);
        Task<List<T>> UpdateSet(List<T> model);
        Task Delete(int id);
        Task WipeClean();
        Task<List<T>> FieldSearch(string field, string[] terms, decimal? min, decimal? max, decimal? exact);
    }
}
