﻿using Amount.CodeExercise.Core.Models.Shared;

namespace Amount.CodeExercise.Core.Models.Loan
{
    public class AnnualPercentageRate : AsValue<decimal>
    {
        public const decimal Min = 4m;
        public const decimal Max = 12m;
        public static implicit operator decimal(AnnualPercentageRate annualPercentageRate)
        {
            return annualPercentageRate.Value;
        }

        public static implicit operator AnnualPercentageRate(decimal value)
        {
            if (value < Min)
            {
                throw new ArgumentOutOfRangeException(nameof(AnnualPercentageRate), $"Cannot be less than {Min}%");
            }
            if (value > Max)
            {
                throw new ArgumentOutOfRangeException(nameof(AnnualPercentageRate), $"Cannot be more than {Max}%");
            }
            return new AnnualPercentageRate()
            {
                Value = value
            };
        }
    }
}
