﻿using Amount.CodeExercise.Core.Models.Business;
using Amount.CodeExercise.Core.Models.Demographic;
using Amount.CodeExercise.Core.Models.Loan;
using System.Reflection;
using System.Text.Json.Serialization;

namespace Amount.CodeExercise.Api.DataTransferObjects
{
    public class Application
    {
        public int LoanId { get; set; }
        public decimal RequestedAmount { get; set; }
        public decimal AnnualPercentageRate { get; set; }
        public int Term { get; set; }
        public decimal RiskRating { get; set; }

        public string OrganizationName { get; set; }
        public int CreditRating { get; set; }
        public decimal OutstandingDebt { get; set; }
        public int RecentDefaults { get; set; }
        public decimal Revenue { get; set; }
        public string EmployeeIdentificationNumber { get; set; }

        public string SignerFirstName { get; set; }
        public string SignerLastName { get; set; }
        public DateTime SignerDateOfBirth { get; set; }
        public string SignerSocialSecurityNumber { get; set; }

        [JsonIgnore]
        public string SearchText { get; private set; } = string.Empty;

        public Application()
        {

        }

        public Application(Loan loan, Organization organization, Person person)
        {
            LoanId = loan.Id;
            RequestedAmount = loan.RequestedAmount;
            AnnualPercentageRate = loan.AnnualPercentageRate;
            Term = loan.Term;
            RiskRating = loan.RateRisk(organization);

            OrganizationName = organization.Name;
            CreditRating = organization.CreditRating;
            OutstandingDebt = organization.OutstandingDebt;
            RecentDefaults = organization.RecentDefaults;
            Revenue = organization.Revenue;
            EmployeeIdentificationNumber = organization.EmployeeIdentificationNumber;

            SignerFirstName = person.FirstName;
            SignerLastName = person.LastName;
            SignerDateOfBirth = person.DateOfBirth;
            SignerSocialSecurityNumber = person.SocialSecurityNumber;

            SearchText = string.Empty;
            foreach (var prop in GetType().GetProperties())
            {
                SearchText += prop.GetValue(this)?.ToString().ToLower() + " ";
            }
        }

        public object? ValueOf(string property)
        {
            return Properties.ContainsKey(property) ? Properties[property].GetValue(this) : null;
        }

        public static Dictionary<string,PropertyInfo> Properties = typeof(Application).GetProperties().ToDictionary(p => p.Name.ToLower());
        
    }
}
