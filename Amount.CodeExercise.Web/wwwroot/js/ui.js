﻿import { network } from './../lib/pianowire.js';
import { Application } from './models/application.js';
import { fields, renderer } from './models/fields.js';
network.host = window.location.origin;

const globals = {},
    data = {
    applications: [],
    filtered: []
};

async function run() {

    function addListeners() {
        document.listen('submit', anySubmit);
        document.listen('click', anyclick);
        document.listen('input', anyInput);
    }
    function anySubmit(e) {
        if (e.submitter?.classList.contains('retreat')) {
            e.preventDefault();
            return false;
        }
        switch (e.target.attr('name')) {
            case 'person': {
                gotoStep('org');
                e.preventDefault();
                return false;
            }
            case 'organization': {
                gotoStep('loan');
                e.preventDefault();
                return false;
            }
            case 'loan': {
                e.preventDefault();
                submitCreate();
                return false;
            }
        }
    }
    function anyclick(e) {
        if (e.target.id.contains('seed-data')) {
            seedData();
        } else if (e.target.id.contains('go-create')) {
            openCreate(e);
        } else if (e.target.nodeName === 'STEP') {
            gotoStep(e.target.dataset.target)
        } else if (e.target.classList.contains('retreat')) {
            cancelCreate();
        } else if (e.target.classList.contains('edit')) {
            openEdit(e.target.dataset.loanid);
        } else if (e.target.classList.contains('delete')) {
            promptDelete(e.target.dataset.loanid);
        } else if (e.target.id === 'go-search') {
            //quickFilter($('#keyword').value);
            serverSearch();
        } else if (e.target.id === 'go-clear') {
            clearSearch();
        } else if (e.target.id === 'clear-data') {
            clearData();
        } else if (e.target.nodeName === 'TH' && e.target.dataset.field) {
            if (e.target.isActive()) {
                switchSort(e.target.dataset.field);
            } else {
                sortOnColumn(e.target.dataset.field);
            }            
        }
        else if (e.target.id === 'go-hide-message') {
            'applications message'.deactivate();
        }
    }
    var idle = true;
    async function anyInput(e) {
        if (e.target.dataset.action === 'sort') {
            if (data.filtered.length) serverSearch()
            else await refreshData();
            updateSortDisplay();
        } else if (e.target.dataset.action === 'search') {
            if (e.target.id === 'adv-field') {
                clearSearch();
                showSearchOptions();
                return;
            }
            if (idle) {
                idle = false;
                setTimeout(async () => { await serverSearch(); idle = true; },2500);
            }
        }
    }
    async function serverSearch() {
        'spinner'.activate();
        let query = [],
            field = $('#adv-field').value;
        if (field && fields[field].numeric) {
            if ($('#adv-exact').value) {
                query.push(`exact=${$('#adv-exact').value}`);
            }
            if ($('#adv-min').value) {
                query.push(`min=${$('#adv-min').value}`);
            }
            if ($('#adv-max').value) {
                query.push(`max=${$('#adv-max').value}`);
            }
        }
        else {
            if ($('#keyword').value) {
                query.push(`keyword=${$('#keyword').value}`);
            }
        }        

        if (!query.length) {
            return await refreshData();
        }

        if (field) {
            query.push(`field=${field}`);
        }

        query.push(`sort=${$('#sort-field').value || fields.riskRating.key}`);
        query.push(`direction=${$('#sort-direction').value || 'asc'}`);        

        let matches = await network.get(`api/application/search?${query.join('&')}`);
        if (matches.error) {
            handleError('Error searching', matches.message, 5000);
        }
        data.filtered = matches?.map(a => new Application().fromDto(a));
        renderTable(data.filtered);
        'spinner'.deactivate();
    }
    function clearSearch() {
        $$('search input').forEach(i => i.value = '');
        renderTable(data.applications);
    }
    async function seedData() {
        if (window.confirm('This will erase any existing data.  Are you sure?')) {
            'spinner'.activate();
            var response = await network.post('api/application/seed');
            if (response.error) {
                handleError('Error seeding data', response.message, 5000);
            }
            await refreshData();
        }
    }
    async function clearData() {
        'spinner'.activate();
        await network.delete('api/application/all');
        refreshData();
    }
    function openCreate(e) {
        'create'.activate();
        'create nav step'.activate();
        'create person'.activate();
    }
    function cancelCreate() {
        $('#keyword').focus();
        'create'.deactivate();
        'create nav step.active'.deactivate();
        'create .panel.active'.deactivate();
    }
    function openEdit(loanId) {
        openCreate();
        let application = data.applications.find(a => loanId == a.loanId);
        Object.keys(application).forEach(key => {
            if ($(`input[name=${key}`)) {
                $(`input[name=${key}`).value = key === 'signerDateOfBirth' ? application[key]?.split('T')[0] : application[key];
            }
        });
    }
    async function promptDelete(loanId) {
        if (window.confirm(`Will delete record (loan Id ${loanId}. Please confirm.`)) {
            'spinner'.activate();
            let response = await network.delete(`api/application/${loanId}`);
            if (response.error) {
                handleError('Error deleting application', response.message, 5000);
            }
            $(`#loan-${loanId}`).remove();
            'spinner'.deactivate();
        }
    }
    function isInvalid() {
        for (let form of $$('form')) {
            if (!form.checkValidity()) {
                console.log('invalid form', form)
                return true;
            }
        }
        return false;
    }
    async function submitCreate() {
        let application = new Application().fromCreate();
        console.log('submitting', application, application.toDto());
        if (isInvalid()) {
            alert('There are some errors on the form; please fix them before submitting');
            return;
        }
        cancelCreate();
        'spinner'.activate();
        var response = application.loanId > 0 ? await network.put('api/application', application.toDto()) : await network.post('api/application', application.toDto());
        if (response.error) {
            handleError('Error saving application', response.message, 5000);
        }
        await refreshData();
    }
    function gotoStep(step) {
        'nav step.person'.deactivate();
        'nav step.org'.deactivate();
        'nav step.loan'.deactivate();
        '.panel.active'.deactivate();
        if (step === 'loan') {
            'loan'.activate();
            `step.${step}`.activate();
        } else if (step == 'org') {
            'organization'.activate();
            `step.${step}`.activate();
        } else {
            'person'.activate();
            'nav step'.activate();
        }
    }
    async function refreshData() {
        'spinner'.activate();
        data.filtered = [];
        await getData();
        if (data.applications?.length) {
            renderTable(data.applications);
        } else {
            renderNoData();
        }
        'spinner'.deactivate();

    }
    async function getData() {
        let response = await network.get(`api/application?sort=${'#sort-field'.value || fields.riskRating.key}&direction=${'#sort-direction'.value}`);
        if ((response.error && response.message?.contains('0x80131904')) || ((typeof response == 'string') && response.contains('0x80131904'))) {
            handleError('SQL Server Connection Error', 'It looks like we can\'t connect to SQL server.  Please verify the connection string is set correctly in appsettings.json');
        }
        if (response.error) {
            handleError('Error getting applications', response.message, 5000);
        }
        data.applications = response?.map(a => new Application().fromDto(a));
    }
    function renderTable(applications) {
        'nonecreated'.deactivate();
        'actions'.activate();
        'search'.activate();
        'sort'.activate();
        'applications table tbody'.html = renderer.headerTop() + renderer.headerMain() + applications.map(renderer.row).join('')
        'applications table'.activate();
        'applications message'.html = `Showing ${applications.length} of ${data.applications.length} records <button id='go-hide-message'><box>[-]</box> hide</button>`;
        'applications message'.activate();
        setTimeout(() => 'applications message'.deactivate(),5000);
    }
    function renderNoData() {
        'actions'.deactivate();
        'search'.deactivate();
        'sort'.deactivate();
        'applications table'.deactivate();
        'nonecreated'.activate();
    }
    function handleError(title, message, timeout) {
        'spinner'.deactivate();
        $('error h2').innerText = title;
        $('error message').innerText = message || 'The server encountered an error performing this action';
        'error'.activate();

        if (!!timeout) {
            setTimeout(() => 'error'.deactivate(), timeout);
        }
    }
    function initializeOtherDisplayThings() {

        $('#adv-field').innerHTML = '<option value="">(any field)</option>' + renderer.options();
        $('#sort-field').innerHTML = renderer.options();
        '#sort-field'.value = fields.riskRating.key;
        showSearchOptions();
        updateSortDisplay();
    }
    function showSearchOptions() {
        let field = '#adv-field'.value;
        if (field && fields[field].numeric) {
            'textsearch'.deactivate();
            'numericsearch'.activate();
        } else {
            'numericsearch'.deactivate();
            'textsearch'.activate();            
        }
    }
    function updateSortDisplay() {
        $(`th.asc`)?.classList.remove('asc');
        $(`th.desc`)?.classList.remove('desc');
        `th.active`.deactivate();
        `th.${'#sort-field'.value}`.activate().classList.add('#sort-direction'.value);
    }
    async function sortOnColumn(field) {
        if (fields[field]) {
            '#sort-field'.value = field;
            'th.active'.deactivate();
            await refreshData();
            updateSortDisplay();
        }        
    }
    async function switchSort(field) {
        if ('#sort-direction'.value === 'asc') '#sort-direction'.value = 'desc';
        else '#sort-direction'.value = 'asc';
        if (data.filtered.length) await serverSearch();
        else await refreshData();
        `th.${field}`.activate().classList.add('#sort-direction'.value);
    }
    async function initialize() {
        globals.onError = handleError;
        'spinner'.activate();
        addListeners();
        await refreshData();
        initializeOtherDisplayThings();
        'spinner'.deactivate();
    }

    initialize();
}

try {
    run();
}
catch (err) {
    console.log('runtime error',err);
    globals.onError('An error was encountered', JSON.stringify(err), 7000);
}

