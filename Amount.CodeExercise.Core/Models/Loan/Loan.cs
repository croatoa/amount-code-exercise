﻿using Amount.CodeExercise.Core.Models.Business;
using Amount.CodeExercise.Core.Models.Shared;

namespace Amount.CodeExercise.Core.Models.Loan
{
    public class Loan : BaseModel
    {
        public int PersonId { get; set; }
        public int OrganizationId { get; set; }
        public RequestedAmount RequestedAmount { get; set; } = RequestedAmount.Min;
        public Term Term { get; set; } = Term.Min;
        public AnnualPercentageRate AnnualPercentageRate { get; set; } = AnnualPercentageRate.Min;


        public RiskRating RateRisk(Organization organization)
        {
            decimal risk;

            risk = (CreditRating.Max - organization.CreditRating) / (decimal)organization.CreditRating * (organization.RecentDefaults + 1);

            risk += (AnnualPercentageRate / AnnualPercentageRate.Max) * (Term / Term.Max);

            if (organization.Revenue < 0)
            {
                // add by scale for each significant figure in the red
                risk += ((int)((Math.Abs(organization.Revenue) + organization.OutstandingDebt + RequestedAmount))).ToString().Length;
            }
            else
            {
                risk += (organization.OutstandingDebt + RequestedAmount) / organization.Revenue;
            }

            return risk;
        }
    }
}
