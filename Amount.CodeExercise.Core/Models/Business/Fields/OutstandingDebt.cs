﻿using Amount.CodeExercise.Core.Models.Shared;

namespace Amount.CodeExercise.Core.Models.Business
{
    public class OutstandingDebt : AsValue<decimal>
    {
        public const decimal Max = 1000000m;
        public const decimal Min = 25000m;
        public static implicit operator decimal(OutstandingDebt outstandingDebt)
        {
            return outstandingDebt.Value;
        }

        public static implicit operator OutstandingDebt(decimal value)
        {
            if (value < Min)
            {
                throw new ArgumentOutOfRangeException(nameof(OutstandingDebt), $"Cannot be less than {Min.ToString("c")}");
            }
            if (value > Max)
            {
                throw new ArgumentOutOfRangeException(nameof(OutstandingDebt), $"Cannot be more than {Max.ToString("c")}");
            }
            return new OutstandingDebt()
            {
                Value = value
            };
        }
    }
}
