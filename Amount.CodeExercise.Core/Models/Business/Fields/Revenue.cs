﻿using Amount.CodeExercise.Core.Models.Shared;

namespace Amount.CodeExercise.Core.Models.Business
{
    public class Revenue : AsValue<decimal>
    {
        public const decimal Min = -1000000m;
        public const decimal Max = 1000000m;
        public static implicit operator decimal(Revenue revenue)
        {
            return revenue.Value;
        }

        public static implicit operator Revenue(decimal value)
        {
            return new Revenue()
            {
                Value = value
            };
        }
    }
}
