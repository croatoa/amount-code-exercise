﻿namespace Amount.CodeExercise.Api.DataTransferObjects
{
    public class Query
    {
        public string[] Keywords;
        public string Field;
        public decimal? Min;
        public decimal? Max;
        public decimal? Exact;
        public string Sort;
        public string Direction;

        public Query(string? keyword, string? field, decimal? min, decimal? max, decimal? exact, string? sort, string? direction)
        {
            Keywords = string.IsNullOrWhiteSpace(keyword) ? new string[0] :  keyword.ToLower().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            Field = string.IsNullOrWhiteSpace(field) ? string.Empty : field.ToLower();
            Min = min;
            Max = max;
            Exact = exact;
            Sort = sort ?? "riskrating";
            Direction = direction ?? "asc";
        }
    }
}
