﻿namespace Amount.CodeExercise.Core.Models.Shared
{
    public class BaseModel
    {
        public int Id { get; set; }

        public static int GetId(object o)
        {
            if (o == null)
            {
                return 0;
            }
            return (o as BaseModel).Id;
        }
    }
}
