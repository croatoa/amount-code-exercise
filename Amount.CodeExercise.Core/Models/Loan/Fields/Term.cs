﻿using Amount.CodeExercise.Core.Models.Shared;

namespace Amount.CodeExercise.Core.Models.Loan
{
    public class Term : AsValue<int>
    {
        public const int Min = 1;
        public const int Max = 360;
        public static implicit operator int(Term term)
        {
            return term.Value;
        }

        public static implicit operator Term(int value)
        {
            if (value < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(Term), $"Cannot be less than {Min} month");
            }
            if (value > 360)
            {
                throw new ArgumentOutOfRangeException(nameof(Term), $"Cannot be greater than {Max} months (30 years)");
            }
            return new Term()
            {
                Value = value
            };
        }
    }
}