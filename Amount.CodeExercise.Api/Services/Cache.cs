﻿using Amount.CodeExercise.Api.Interfaces;

namespace Amount.CodeExercise.Api.Services
{
    public class Cache<T> : ICache<T> where T : class
    {
        public List<T> Read()
        {
            if (IsSet)
            {
                return Data.Things;
            }
            else
            {
                throw new Exception($"{typeof(T).Name} has not been cached");
            }
        }
        public void Write(List<T> things)
        {
            Data.Things = things;
            LastWrite = DateTime.UtcNow;
            IsSet = true;
        }

        public void Update(T thing)
        {
            if (IsSet)
            {
                if (Data.Things.Contains(thing))
                {
                    Data.Things[Data.Things.IndexOf(thing)] = thing;
                }
                else
                {
                    Add(thing);
                }
            }
            else
            {
                Data.Things = new List<T>() { thing };
                IsSet = true;
            }
        }

        public void Add(T thing)
        {
            if (IsSet)
            {
                Data.Things.Add(thing);
            }
            else
            {
                Data.Things = new List<T>() { thing };
                IsSet = true;
            }
        }

        public void Invalidate()
        {
            Data.Things?.Clear();
            IsSet = false;
        }

        public DateTime? LastWrite { get; private set; }
        public bool IsSet { get; private set; }

        static class Data
        {
            public static List<T> Things = null;
        }
    }

}
