﻿
import { fields } from './fields.js';

class Application {
    constructor() {
        this.loanId = null;
        this.requestedAmount = null;
        this.annualPercentageRate = null;
        this.term = null;
        this.riskRating = null;
        this.organizationName = null;
        this.creditRating = null;
        this.outstandingDebt = null;
        this.recentDefaults = null;
        this.revenue = null;
        this.employeeIdentificationNumber = null;
        this.signerFirstName = null;
        this.signerLastName = null;
        this.signerDateOfBirth = null;
        this.signerSocialSecurityNumber = null;
    }
    fromDto(dto) {
        Object.keys(dto).forEach(key => this[key] = dto[key]);
        return this;
    }
    fromCreate() {
        Object.keys(this).forEach(key => {
            this[key] = document.querySelector(`input[name=${key}`)?.value;
        });
        return this;
    }
    toDto() {
        let dto = {};
        Object.keys(this).forEach(key => {
            dto[key] = this[key];
        });
        return dto;
    }
    toHtml(keys) {
        return `<tr id='loan-${this.loanId}' class='loan'>${keys.map(key => `<td style='${(fields[key].visible ? '' : 'display:none;')}'>${fields[key].format(this[key])}</td>`).join('')}<td class='edit' data-loanid='${this.loanId}'>EDIT</td><td class='delete' data-loanid='${this.loanId}'>DELETE</td></tr>`;
    }
}

export { Application }