﻿namespace Amount.CodeExercise.Core.Models.Shared
{
    public class Contact
    {
        public string Phone { get; set; }
        public string Email { get; set; }
        public Address PhysicalAddress { get; set; }

        public class Address
        {
            public string LineOne { get; set; } = string.Empty;
            public string LineTwo { get; set; } = string.Empty;
            public string City { get; set; } = string.Empty;
            public string State { get; set; } = string.Empty;
            public string ZipCode { get; set; } = string.Empty;
        }
    }
}
