﻿using Amount.CodeExercise.Core.Models.Shared;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Amount.CodeExercise.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public ValidationParameters Validations => new ValidationParameters();
        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {

        }
    }
}