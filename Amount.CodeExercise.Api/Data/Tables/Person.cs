﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Models = Amount.CodeExercise.Core.Models.Demographic;

namespace Amount.CodeExercise.Api.Data.Tables
{
    public class Person
    {
        [Key]
        [Column(Order = 0)]
        public int Id { get; set; }
        [Column(TypeName = "varchar(26)")]
        public string FirstName { get; set; }
        [Column(TypeName = "varchar(26)")]
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        [Column(TypeName = "varchar(9)")]
        public string SocialSecurityNumber { get; set; }

        public Models.Person ToModel()
        {
            return new Models.Person()
            {
                Id = Id,
                FirstName = FirstName,
                LastName = LastName,
                SocialSecurityNumber = SocialSecurityNumber,
                DateOfBirth = DateOfBirth
            };
        }

        public static Person FromModel(Models.Person person)
        {
            if (person == null)
            {
                return new Person();
            }
            return new Person()
            {
                Id = person.Id,
                FirstName = person.FirstName,
                LastName = person.LastName,
                SocialSecurityNumber = person.SocialSecurityNumber,
                DateOfBirth = person.DateOfBirth
            };
        }
    }
}
