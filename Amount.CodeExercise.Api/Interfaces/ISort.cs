﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amount.CodeExercise.Api.Interfaces
{
    public interface ISort<TObj, TKey> where TObj : class
    {
        Type TableType { get; }
        bool IsDescending { get; }
        Func<TObj, TKey> Lambda { get; }
    }
}
