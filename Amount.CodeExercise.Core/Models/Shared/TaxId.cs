﻿namespace Amount.CodeExercise.Core.Models.Shared
{
    public class TaxId : AsValue<string>
    {
        public const int Exact = 9;
        public const int Min = 100000000;
        public const int Max = 999999999;

        public static implicit operator string(TaxId taxId)
        {
            return taxId.Value ?? string.Empty;
        }

        public static implicit operator TaxId(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException(nameof(TaxId), "Cannot cast null to Tax Id");
            }
            if (value.Length != Exact)
            {
                throw new ArgumentException($"Tax Id must be {Exact} characters");
            }
            return new TaxId()
            {
                Value = value
            };
        }
    }
}
