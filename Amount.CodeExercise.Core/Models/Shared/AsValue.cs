﻿namespace Amount.CodeExercise.Core.Models.Shared
{
    public abstract class AsValue<T>
    {
        protected virtual T? Value { get; set; }

        public override bool Equals(object? obj)
        {
            return obj != null && obj is AsValue<T> && (Value?.GetHashCode() == (obj as AsValue<T>).Value?.GetHashCode());
        }
        public override int GetHashCode()
        {
            return (Value?.ToString() ?? string.Empty).GetHashCode();
        }
    }
}
