﻿using Amount.CodeExercise.Core.Models.Shared;

namespace Amount.CodeExercise.Core.Models.Business
{
    public class CreditRating : AsValue<int>
    {
        public const int Max = 750;
        public const int Min = 600;

        public static implicit operator int(CreditRating creditRating)
        {
            return creditRating.Value;
        }

        public static implicit operator CreditRating(int value)
        {
            if (value < Min)
            {
                throw new ArgumentOutOfRangeException(nameof(CreditRating), $"Cannot be less than {Min}");
            }
            if (value > Max)
            {
                throw new ArgumentOutOfRangeException(nameof(CreditRating), $"Cannot be more than {Max}");
            }
            return new CreditRating()
            {
                Value = value
            };
        }
    }
}
