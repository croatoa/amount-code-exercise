﻿using Amount.CodeExercise.Api.DataTransferObjects;

namespace Amount.CodeExercise.Api
{
    public static class Utilities
    {
        public static List<Application> SortDtos(this List<Application> dtos, string sortField = "riskrating", string direction = "asc")
        {
            sortField = sortField.ToLower();
            direction = direction.ToLower();
            if (direction == "desc")
            {
                return dtos.OrderByDescending(dto => dto.ValueOf(sortField)).ToList();
            }
            else
            {
                return dtos.OrderBy(dto => dto.ValueOf(sortField)).ToList();
            }

        }
    }
}
