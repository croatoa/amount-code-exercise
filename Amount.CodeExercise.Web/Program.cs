using Amount.CodeExercise.Api.Controllers;
using Amount.CodeExercise.Api.Data;
using Amount.CodeExercise.Api.Interfaces;
using Amount.CodeExercise.Api.Services;
using Microsoft.EntityFrameworkCore;
var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<CodeExerciseContext>(
  options =>
  {
      options.UseSqlServer(builder.Configuration.GetConnectionString("AmountCodeExercise"));
  }
);

builder.Services.AddScoped<ILogger<ApplicationService>, Logger<ApplicationService>>();
builder.Services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
builder.Services.AddScoped<IApplicationService, ApplicationService>();

builder.Services.AddControllers().AddApplicationPart(typeof(ApplicationController).Assembly);

builder.Services.AddRazorPages();

var app = builder.Build();

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();

app.UseEndpoints(endpoints => endpoints.MapControllers());

app.MapRazorPages();
app.MapGet("/ping", () => "pong!");

app.Run();
