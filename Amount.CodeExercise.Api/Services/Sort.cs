﻿using Amount.CodeExercise.Api.Interfaces;

namespace Amount.CodeExercise.Api
{
    public class Sort<TObj, TKey> : ISort<TObj,TKey> where TObj : class
    {
        public Type TableType { get; private set; }
        public bool IsDescending { get; private set; }
        public Func<TObj, TKey> Lambda { get; private set; }

        public Sort(Func<TObj, TKey> lambda, bool isDescending = false)
        {
            TableType = typeof(TObj);
            Lambda = lambda;
            IsDescending = isDescending;
        }
    }
}
