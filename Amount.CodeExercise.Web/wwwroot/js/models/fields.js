﻿const fields = {
    loanId: {
        key: 'loanId',
        label: 'Loan Id',
        visible: false,
        editable: false,
        format: x => x,
        numeric: true
    },
    organizationName: {
        key: 'organizationName',
        label: 'Name',
        visible: true,
        editable: true,
        format: x => x,
        numeric: false
    },
    employeeIdentificationNumber: {
        key: 'employeeIdentificationNumber',
        label: 'Federal Tax Id (EIN)',
        visible: true,
        editable: true,
        format: x => x,
        numeric: false
    },
    revenue: {
        key: 'revenue',
        label: 'Revenue',
        visible: true,
        editable: true,
        format: x => Number(x).toFixed(2),
        numeric: true

    },
    creditRating: {
        key: 'creditRating',
        label: 'Credit Rating',
        visible: true,
        editable: true,
        format: x => x,
        numeric: true
    },
    outstandingDebt: {
        key: 'outstandingDebt',
        label: 'Current Debt',
        visible: true,
        editable: true,
        format: x => Number(x).toFixed(2),
        numeric: true
    },
    recentDefaults: {
        key: 'recentDefaults',
        label: 'Defaults (last 5 years)',
        visible: true,
        editable: true,
        format: x => x,
        numeric: true
    },
    signerFirstName: {
        key: 'signerFirstName',
        label: 'First Name',
        visible: true,
        editable: true,
        format: x => x,
        numeric: false
    },
    signerLastName: {
        key: 'signerLastName',
        label: 'Last Name',
        visible: true,
        editable: true,
        format: x => x,
        numeric: false
    },
    signerDateOfBirth: {
        key: 'signerDateOfBirth',
        label: 'DOB',
        visible: true,
        editable: true,
        format: x => x.split('T')[0],
        numeric: false
    },
    signerSocialSecurityNumber: {
        key: 'signerSocialSecurityNumber',
        label: 'SSN',
        visible: true,
        editable: true,
        format: x => `***-**-${x.substring(5)}`,
        numeric: false
    },
    annualPercentageRate: {
        key: 'annualPercentageRate',
        label: 'APR',
        visible: true,
        editable: true,
        format: x => `${Number(x).toFixed(2)}%`,
        numeric: true
    },
    term: {
        key: 'term',
        label: 'Term',
        visible: true,
        editable: true,
        format: x => x,
        numeric: true
    },
    requestedAmount: {
        key: 'requestedAmount',
        label: 'Amount',
        visible: true,
        editable: true,
        format: x => Number(x).toFixed(2),
        numeric: true
    },
    riskRating: {
        key: 'riskRating',
        label: 'Risk Rating',
        visible: true,
        editable: false,
        format: x => Number(x || 0)?.toFixed(2),
        numeric: true
    }
}
const keys = Object.keys(fields),
    values = Object.values(fields);

const renderer = {
    options: () => values.filter(c => c.visible).map(f => `<option value='${f.key}'>${f.label}</option>`).join(''),
    headerTop: () => `<tr class='top'><th colspan='6'>Organization</th><th colspan='4'>Representative</th><th colspan='4'>Loan</th><th colspan='2'>Actions</th></tr>`,
    headerMain: () => `<tr>${values.map(c => `<th class='${c.key}' style='${(c.visible ? '' : 'display:none;')}' data-field='${c.key}' >${c.label}</th>`).join('')}<th colspan='2'>&nbsp;</th></tr>`,
    row: la => `<tr id='loan-${la.loanId}' class='loan'>${keys.map(key => `<td style='${(fields[key].visible ? '' : 'display:none;')}'>${fields[key].format(la[key])}</td>`).join('')}<td class='edit' data-loanid='${la.loanId}'>EDIT</td><td class='delete' data-loanid='${la.loanId}'>DELETE</td></tr>`
}
export { fields, renderer }