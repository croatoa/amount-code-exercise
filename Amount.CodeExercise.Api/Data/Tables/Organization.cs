﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Models = Amount.CodeExercise.Core.Models.Business;

namespace Amount.CodeExercise.Api.Data.Tables
{
    public class Organization
    {
        [Key]
        [Column(Order = 0)]
        public int Id { get; set; }
        [Column(TypeName = "varchar(52)")]
        public string Name { get; private set; }
        public int CreditRating { get; private set; }
        public decimal OutstandingDebt { get; private set; }
        public int RecentDefaults { get; private set; }
        public decimal Revenue { get; private set; }
        [Column(TypeName = "varchar(9)")]
        public string EmployeeIdentificationNumber { get; private set; }

        public Models.Organization ToModel()
        {
            return new Models.Organization()
            {
                Id = Id,
                Name = Name,
                CreditRating = CreditRating,
                OutstandingDebt = OutstandingDebt,
                RecentDefaults = RecentDefaults,
                Revenue = Revenue,
                EmployeeIdentificationNumber = EmployeeIdentificationNumber
            };
        }

        public static Organization FromModel(Models.Organization organization)
        {
            if (organization == null)
            {
                return new Organization();
            }
            return new Organization()
            {
                Id = organization.Id,
                EmployeeIdentificationNumber = organization.EmployeeIdentificationNumber,
                Name = organization.Name,
                CreditRating = organization.CreditRating,
                OutstandingDebt = organization.OutstandingDebt,
                RecentDefaults = organization.RecentDefaults,
                Revenue = organization.Revenue
            };
        }

    }
}
