﻿using Amount.CodeExercise.Api.DataTransferObjects;
using Amount.CodeExercise.Api.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Amount.CodeExercise.Api.Controllers
{
    [ApiController]
    [Route("api")]
    public class ApplicationController : ControllerBase
    {
        private const int _accepted = 202;

        private IApplicationService _applicationService;
        public ApplicationController(IApplicationService applicationService)
        {
            _applicationService = applicationService;
        }

        [HttpGet]
        [Route("application/{loanId}")]
        public async Task<JsonResult> GetApplicationAsync(int loanId)
        {
            return await Handle(_applicationService.GetApplicationAsync(loanId));
        }

        [HttpGet]
        [Route("application")]
        public async Task<JsonResult> GetApplicationsAsync(string? sort, string? direction)
        {
            return await Handle(_applicationService.GetApplicationsAsync(sort,direction));
        }

        [HttpPost]
        [Route("application")]
        public async Task<ActionResult> CreateApplicationAsync(Application application)
        {
            return await Handle(_applicationService.CreateApplicationAsync(application));
        }

        [HttpPut]
        [Route("application")]
        public async Task<ActionResult> EditApplicationAsync(Application application)
        {
            return await Handle(_applicationService.EditApplicationAsync(application));
        }

        [HttpDelete]
        [Route("application/{loanId:int}")]
        public async Task<ActionResult> DeleteApplicationAsync(int loanId)
        {
            return await Handle(_applicationService.DeleteApplicationAsync(loanId));
        }


        [HttpDelete]
        [Route("application/all")]
        public async Task<ActionResult> DeleteApplicationAsync()
        {
            return await Handle(_applicationService.DeleteApplicationAsync());
        }

        [HttpPost]
        [Route("application/seed")]
        public async Task<ActionResult> SeedData()
        {
            return await Handle(_applicationService.SeedData());
        }

        [HttpGet]
        [Route("application/search")]
        public async Task<JsonResult> Search(string? keyword = null, string? field = null, decimal? min = null, decimal? max = null, decimal? exact = null, string? sort = "riskrating", string? direction = "asc")
        {
            return await Handle(_applicationService.Search(new Query(keyword, field, min, max, exact,sort,direction)));
        }

        private async Task<ActionResult> Handle(Task action)
        {
            try
            {
                await action;
                return StatusCode(_accepted);
            }
            catch (Exception exception)
            {
                return new JsonResult(new
                {
                    error = true,
                    status = 500,
                    message = $"{exception.Message};{Environment.NewLine}{exception.InnerException?.Message}",
                }
                );
            }
        }

        private async Task<JsonResult> Handle<T>(Task<T> action)
        {
            try
            {
                return new JsonResult(await action);
            }
            catch (Exception exception)
            {
                return new JsonResult(new
                {
                    error = true,
                    status = 500,
                    message = $"{exception.Message};{Environment.NewLine}{exception.InnerException?.Message}",
                }
                );
            }
        }
    }
}
