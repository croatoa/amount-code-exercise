﻿namespace Amount.CodeExercise.Api.Interfaces
{
    public interface ICache<T> where T : class
    {
        List<T> Read();
        void Write(List<T> things);
        void Add(T thing);
        void Update(T thing);
        void Invalidate();
        DateTime? LastWrite { get; }
        bool IsSet { get; }
    }
}
