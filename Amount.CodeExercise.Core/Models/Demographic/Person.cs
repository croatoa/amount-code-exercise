﻿using Amount.CodeExercise.Core.Models.Shared;

namespace Amount.CodeExercise.Core.Models.Demographic
{
    public class Person : BaseModel
    {

        public Name FirstName { get; set; }
        public Name LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public TaxId SocialSecurityNumber { get; set; }
    }
}
