﻿using Amount.CodeExercise.Core.Models.Shared;

namespace Amount.CodeExercise.Core.Models.Business
{
    public class RecentDefaults : AsValue<int>
    {
        public const int Min = 0;
        public const int Max = 60;

        public static implicit operator int(RecentDefaults recentDefaults)
        {
            return recentDefaults.Value;
        }

        public static implicit operator RecentDefaults(int value)
        {
            if (value < Min)
            {
                throw new ArgumentOutOfRangeException(nameof(RecentDefaults), $"Cannot be fewer than {Min}");
            }
            return new RecentDefaults()
            {
                Value = value
            };
        }
    }
}
