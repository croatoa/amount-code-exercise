﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Models = Amount.CodeExercise.Core.Models.Loan;

namespace Amount.CodeExercise.Api.Data.Tables
{
    public class Loan
    {
        [Key]
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int OrganizationId { get; set; }
        public decimal RequestedAmount { get; set; }
        public decimal AnnualPercentageRate { get; set; }
        public int Term { get; set; }

        public Models.Loan ToModel()
        {
            return new Models.Loan()
            {
                Id = Id,
                PersonId = PersonId,
                OrganizationId = OrganizationId,
                Term = Term,
                AnnualPercentageRate = AnnualPercentageRate,
                RequestedAmount = RequestedAmount
            };
        }
        public static Loan FromModel(Models.Loan loan)
        {
            if (loan == null)
            {
                return new Loan();
            }

            return new Loan()
            {
                Id = loan.Id,
                PersonId = loan.PersonId,
                OrganizationId = loan.OrganizationId,
                RequestedAmount = loan.RequestedAmount,
                AnnualPercentageRate = loan.AnnualPercentageRate,
                Term = loan.Term
            };
        }
    }
}
